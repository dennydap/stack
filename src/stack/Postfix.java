package stack;

import java.io.*;
import java.util.*;
import java.lang.Character;

public class Postfix {
	static char[] inputArray;
	public static Scanner input;
	
	static ArrayList<Character> output = new ArrayList();;
	public static int tinggiPostfix;
	public static int counter;
        public static int sisaArray = 0;
	public static int pembatas = 1;
        public static int counterOutput = 0;
	
        public void inputSoal(String str) {
            String input;
            String isiConsole;
            System.out.print("Masukkan Notasi Postfix yang diinginkan (akhiri notasi dengan tanda \" ; \" : ");
            System.out.println(str);
            inputArray = str.toCharArray();
		for(int i = 0; i < inputArray.length; i++)
		{
			if(inputArray[i] == '(' || inputArray[i] == ')' || inputArray[i] == ';') 
				{
				counterOutput++;
				}
			else{
				continue;
			}
			
		}

		char[][] tempFinal = new char[inputArray.length][inputArray.length];
		
		for(int i = 0; i < inputArray.length;i++){
			for(int j = 0; j < pembatas; j++){
				tempFinal[j][i] = prosesPerbaris(inputArray,pembatas)[j];
			}
			pembatas++;
		}		
		System.out.println("Notasi yang diamati adalah = ");
		for(int i = 0; i < inputArray.length-1;i++){
			System.out.print(inputArray[i] + " ");
		}
		System.out.println();
		
		prosesHasilFinal(tempFinal);
		
		System.out.println ("Outputnya adalah =");
		for(int i=0; i < inputArray.length-counterOutput;i++){
			System.out.print(output.get(i).toString().replaceAll("[\\,]", "") + ' ');
                }
        }
	
	
	public static void printArray(Object[][] theArray) {
	    for(int i = 0; i < theArray.length; i++) {
	        for(int j = 0; j < theArray.length; j++) {
	            System.out.print(theArray[i][j].toString().replaceAll("[\\[\\]]", "") + " ");
	            
	        }
	        System.out.println();
	    }
	}

	// *** Flipping Array ***
	public static void flipInPlace(Object[][] theArray) {
	    for(int i = 0; i < (theArray.length / 2); i++) {
	        Object[] temp = theArray[i];
	        theArray[i] = theArray[theArray.length - i - 1];
	        theArray[theArray.length - i - 1] = temp;
	    }
	}
	
	public static void prosesHasilFinal(char[][] tempArray)
	{
//		char[][] hasilFinal = new char[tempArray.length][tempArray.length];
//		List<List<Character>> hasilFinal = new ArrayList<List<Character>>(tempArray.length);
//		ArrayList<Character>[][] hasilFinal = new ArrayList[tempArray.length][tempArray.length];
		char [][] hasilFinal = new char [tempArray.length][tempArray.length];
		ArrayList<Character>[][] tempFinal = new ArrayList[tempArray.length][tempArray.length];
		
		
		
		
		for(int i = 0; i < tempArray.length;i++)
		{
			for(int j = 0; j < tempArray.length; j++)
			{
				tempFinal[i][j] = new ArrayList<>();
//				hasilFinal[i][j] = new ArrayList<>();
			}
			
		}
		for(int i = 0; i < tempArray.length;i++)
		{
			for(int j = 0; j < tempArray.length; j++)
			{
				if(tempArray[i][j] == '\u0000')
				{
					tempFinal[i][j].add(' ');
				}
				else
				{
					tempFinal[i][j].add(tempArray[i][j]);	
				}	
			}	
		}
		
		
		
		flipInPlace(tempFinal);
		printArray(tempFinal);

	}
        
	public static void main(String[] args) throws IOException {
		
	}
		public static char[] prosesPerbaris(char[] notasi, int jumlahBaca){
		char[] hasilPerbaris = new char[jumlahBaca];
		Stack inputStack;
		inputStack = new Stack();
		if(!inputStack.isEmpty()) {inputStack.removeAllElements();}
		else{inputStack.removeAllElements();}
		
		for(int i = 0; i < jumlahBaca; i++)
		{
//			System.out.println("Jumlah Baca ke- " + jumlahBaca + "huruf" + notasi[i]);
			if(inputStack.isEmpty() && !Character.isLetterOrDigit(notasi[i]))
			{
				inputStack.push(notasi[i]);		
			}
			else if(notasi[i] =='(' && !Character.isLetterOrDigit(notasi[i]))
			{
				inputStack.push(notasi[i]);
//				prosesOutput(' ', jumlahBaca, notasi[i]); A+B+(C*D^E)+F^G+H;
			}
		
			else if(Character.isLetterOrDigit(notasi[i]))
			{
//				inputStack.push(' ');
				prosesOutput(notasi[i], jumlahBaca);
			}
		
			else if(notasi[i] == ')')
			{
				//System.out.println(inputStack);
				//System.out.println(i);
				
				while(inputStack.isEmpty() == false && Character.valueOf((Character) inputStack.peek()) !='(')
				{
					if(inputStack.isEmpty() == false){
					prosesOutput(Character.valueOf((Character) inputStack.pop()), jumlahBaca);
					}
					else{
						continue;
					}
				}
//				A+B+C+D*(E+F+G*(H-I));
				if(inputStack.isEmpty() == false){
					inputStack.pop();
				}
				else{
					continue;
				}
			
			}
			else if(pickOperator(notasi[i]))
			{
				char teratas = Character.valueOf((Character) inputStack.peek());
				if(teratas == '(' && !Character.isLetterOrDigit(notasi[i]))
				{

					inputStack.push(notasi[i]);

				}
//				else if((cekOP(notasi[i])<=2) && (Character.isLetterOrDigit(notasi[i+1]) && (notasi[i] == '(')) || (i < jumlahBaca))
//				{
//					prosesOutput(notasi[i+1], jumlahBaca);
//					prosesOutput(notasi[i], jumlahBaca);
//					
//					
//				}
				else
				{
					if(Character.isLetterOrDigit(teratas))
					{
//						inputStack.push(' ');
						prosesOutput(notasi[i], jumlahBaca);
					}
					else if(teratas == ' '){
						inputStack.pop();
						inputStack.push(notasi[i]);
					}
					else if(cekOP(teratas) < cekOP(notasi[i]))
					{
						inputStack.push(notasi[i]);
					}
					else if(cekOP(teratas) > cekOP(notasi[i]) || cekOP(teratas) == cekOP(notasi[i]))
					{
							
						while(!inputStack.isEmpty() && pickOperator(teratas)){
							prosesOutput(Character.valueOf((Character) inputStack.pop()), jumlahBaca);
						}
						inputStack.push(notasi[i]);
						
					}
				}
			}
			
			else
			{
				while(!inputStack.isEmpty())
				{
					prosesOutput(Character.valueOf((Character) inputStack.pop()), jumlahBaca);
				}
				break;
			}
		
	}
		
		for(int i = 0; i< inputStack.size(); i++){
			if(!inputStack.isEmpty()){
				hasilPerbaris[i] = Character.valueOf((Character) inputStack.toArray()[i]);
				
			}
			else{
				break;
			}
		}

		return hasilPerbaris;
	}
	
	
	public static void prosesOutput(char hasil, int iterasi)
	{
		if(hasil!='(' && iterasi >= inputArray.length && output.size() < inputArray.length)
		{
				output.add(hasil);
		}
		else{
			
		}
	}
	
	public static boolean pickOperator(char op){
		if(op == '+' || op == '-' || op == '/' || op == '^' || op == '*') {
			return true;
		}
		return false;
	}
	
	static int cekOP(char op){
		switch(op){
			case '+':
			case '-':
				return 0;
			case '*':
			case '/':
				return 1;
			case '^':
				return 2;
			default:
				throw new IllegalArgumentException("Operator "+ op +" tidak ada");
			
		}
	}
}
